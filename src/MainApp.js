import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import ListArea from "./Components/ListArea";
import Sidebar from "./Components/SideBar";

const MainApp = () => {
  return (
    <Router>
      <div className="app gd">
        <Sidebar />
        <Route path='/' exact component={() => <ListArea section='My day' />} />
        <Route path='/myday' component={() => <ListArea section='My Day' />} />
        <Route path='/planned' component={() => <ListArea section='Planned' />} />
        <Route path='/all' component={() => <ListArea section='All' />} />
      </div>
    </Router>
  );
};

export default MainApp;
